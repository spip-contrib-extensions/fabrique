# Plugin Fabrique pour SPIP

Ce plugin génère d'autres plugins !

## 3.2.1 - 2024-11-21

### Fixed

- #7 Afficher les enfants d'un objet via un squelette plutôt que par le biais du pipeline afficher_enfants et tri opérant sur liste d'objets liés aux rubriques
- #33 Trimer la configuration des saisies
- #52: proposer une version future en x.* au lieu de x.y.*
- Ne pas provoquer de fatale si on ne fournit pas de logo
- #49 Inutile de retourner `'saisies'` quand on a une fonction `_saisies()`
- #31 Limiter le handle du sortable à un élément

## 3.0.0

- Compatibilité SPIP 4.0+ (PHP 7.3-8.0)
- Suppression de la catégorie dans les paquet.xml
